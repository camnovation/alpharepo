﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ELearning.Models;
using System.Data.Entity;

namespace ELearning.DAL
{
    public class tblContentsContext : DbContext
    {
        public tblContentsContext() : base("tblContentsContext") { this.Configuration.LazyLoadingEnabled = false; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder) { Database.SetInitializer<tblUsersContext>(null); base.OnModelCreating(modelBuilder); }

        public DbSet<tblContent> tblContent { get; set; }

        public DbSet<tblCourse> tblCourse { get; set; }

    }
}