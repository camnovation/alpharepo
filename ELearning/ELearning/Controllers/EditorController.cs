﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELearning.DAL;
using ELearning.Models;

namespace ELearning.Controllers
{
    public class EditorController : Controller
    {
        private tblContentsContext db = new tblContentsContext();
      
        public ActionResult ContentManagement()
        {
            
            var Çontent = db.tblContent.Include(t => t.tblCourse);
            return View(Çontent.ToList());
        }

        // GET: tblContents/Create
        public ActionResult Create()
        {
           
            return View();
        }
        // GET: tblContents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblContent tblContent = db.tblContent.Find(id);
            if (tblContent == null)
            {
                return HttpNotFound();
            }
            return View(tblContent);
        }
        // GET: tblContents/Create
        public ActionResult NewContent()
        {
            return View();
        }
        // POST: tblContents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewContent([Bind(Include = "id,title,clevel,description,cid,date")] tblContent tblContent)
        {
            if (ModelState.IsValid)
            {
                db.tblContent.Add(tblContent);
                db.SaveChanges();
                return RedirectToAction("ContentManagment");
            }

            return View(tblContent);
        }


        // GET: tblContents/Edit/5
        public ActionResult Edite(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblContent tblContent = db.tblContent.Find(id);
            if (tblContent == null)
            {
                return HttpNotFound();
            }
            return View(tblContent);
        }

        // POST: tblContents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edite([Bind(Include = "id,title,clevel,description,cid")] tblContent tblContent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblContent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("ContentManagement");
            }
            return View(tblContent);
        }
        // GET: tblContents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblContent tblContent = db.tblContent.Find(id);
            if (tblContent == null)
            {
                return HttpNotFound();
            }
            return View(tblContent);
        }

        // POST: tblContents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblContent tblContent = db.tblContent.Find(id);
            db.tblContent.Remove(tblContent);
            db.SaveChanges();
            return RedirectToAction("ContentManagement");
        }

    }
}