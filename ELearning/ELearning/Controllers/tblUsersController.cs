﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ELearning.Models;

namespace ELearning.Controllers
{
    public class tblUsersController : Controller
    {
        private tblUsersContext db = new tblUsersContext();

        // GET: tblUsers
        public ActionResult Index()
        {
            var tblUser = db.tblUser.Include(t => t.tblAdmin).Include(t => t.tblEditor).Include(t => t.tblStudent);
            return View(tblUser.ToList());
        }

        // GET: tblUsers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUser tblUser = db.tblUser.Find(id);
            if (tblUser == null)
            {
                return HttpNotFound();
            }
            return View(tblUser);
        }

        // GET: tblUsers/Create
        public ActionResult Create()
        {
            ViewBag.id = new SelectList(db.tblAdmins, "userId", "userId");
            ViewBag.id = new SelectList(db.tblEditors, "userId", "userId");
            ViewBag.id = new SelectList(db.tblStudents, "userId", "userId");
            return View();
        }

        // POST: tblUsers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id,name,email,password,logInFreq")] tblUser tblUser)
        public ActionResult Create(tblUser tblUser)
        {
            if (ModelState.IsValid)
            {
                db.tblUser.Add(tblUser);
                tblStudent tmp = new tblStudent();
                tmp.userId = tblUser.id;
                db.tblStudents.Add(tmp);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }

            ViewBag.id = new SelectList(db.tblAdmins, "userId", "userId", tblUser.id);
            ViewBag.id = new SelectList(db.tblEditors, "userId", "userId", tblUser.id);
            ViewBag.id = new SelectList(db.tblStudents, "userId", "userId", tblUser.id);
            return View(tblUser);
        }

        // GET: tblUsers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUser tblUser = db.tblUser.Find(id);
            if (tblUser == null)
            {
                return HttpNotFound();
            }
            ViewBag.id = new SelectList(db.tblAdmins, "userId", "userId", tblUser.id);
            ViewBag.id = new SelectList(db.tblEditors, "userId", "userId", tblUser.id);
            ViewBag.id = new SelectList(db.tblStudents, "userId", "userId", tblUser.id);
            return View(tblUser);
        }

        // POST: tblUsers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,name,email,password,logInFreq")] tblUser tblUser)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tblUser).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.id = new SelectList(db.tblAdmins, "userId", "userId", tblUser.id);
            ViewBag.id = new SelectList(db.tblEditors, "userId", "userId", tblUser.id);
            ViewBag.id = new SelectList(db.tblStudents, "userId", "userId", tblUser.id);
            return View(tblUser);
        }

        // GET: tblUsers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            tblUser tblUser = db.tblUser.Find(id);
            if (tblUser == null)
            {
                return HttpNotFound();
            }
            return View(tblUser);
        }

        // POST: tblUsers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            tblUser tblUser = db.tblUser.Find(id);
            db.tblUser.Remove(tblUser);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
