﻿using ELearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Net;
using System.Web.Mvc;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.Owin;

namespace ELearning.Controllers
{
    public class UserAccountController : Controller
    {
        private tblUsersContext db = new tblUsersContext();

        //Get UserAccount/Register
        public ActionResult Register()
        {
            return View();
        }

        //Post UserAccount/Register
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "id,name,email,password,logInFreq")] tblUser tblUser)
        public ActionResult Register(tblUser regUser)
        {
            if (ModelState.IsValid)
            {
                db.tblUser.Add(regUser);
                tblStudent tmp = new tblStudent();
                tmp.userId = regUser.id;
                db.tblStudents.Add(tmp);
                db.SaveChanges();
                return RedirectToAction("Index", "Home");
            }
            return View(regUser);
        }

        //Get UserAccount/Login
        public ActionResult Login()
        {
            return View();
        }

        //POST UserAccount/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(loginUser user)
        {
            if (ModelState.IsValid)
            {
                List<tblUser> allUsers = db.tblUser.ToList();
                foreach(tblUser u in allUsers)
                {
                    if(u.email == user.email && u.password == user.password)
                    {
                        if (u.type == "STUDENT")
                        {
                            return RedirectToAction("Index", "Home");
                        }
                        else if (u.type == "ADMIN")
                        {
                            return RedirectToAction("UserManagement", "Admin");
                        }
                        else if (u.type == "EDITOR")
                        {
                            return RedirectToAction("ContentManagement", "Editor");
                        }
                    }
                }
            }
            return View(user);
        }
    }

    
}