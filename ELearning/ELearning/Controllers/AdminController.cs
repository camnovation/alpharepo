﻿using ELearning.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ELearning.Controllers
{
    public class AdminController : Controller
    {
        private tblUsersContext db = new tblUsersContext();

        public ActionResult UserManagement()
        {
            var tblUser = db.tblUser;
            return View(tblUser.ToList());
        }
    }
}